const fs = require('fs')
const colors = require('colors')
const helpers = require('../../lib/helpers')
const ts = require('../../templates/vue-typescript')
const html = require('../../templates/vue-html')
const scss = require('../../templates/vue-scss')


const nameFormatError = 'Names must follow PascalCase.'

const validateArgs = (Args, pageName) => {
  if (Args.numberOfArgs > 3) {
    console.log(colors.red(`Too many arguments passed!`))
    return false
  }

  if (pageName.match(/[-_/]/)) {
    console.log(colors.red(`${pageName} is not a valid file name. ${nameFormatError}`))
    return false
  }

  if (pageName.match(/[0-9]/g)) {
    console.log(colors.red(`${pageName} is not a valid file name. ${nameFormatError}`))
    return false
  }

  return true
}

/**
 * Used to create a folder and matching files for a new Page
 * @param {string} pageName
 * @param {string} path | path where you want to create the folder
 */
const createNewPage = (pageName, path) => {
  path = (path.substr(path.length - 1) === '/') ? path.split(path.substr(path.length - 1))[0] : path

  const folderPath = `${path}/${pageName}`

  if (!fs.existsSync(folderPath)){
    fs.mkdirSync(folderPath)
    console.log(colors.green(`Created ${folderPath}`))

    fs.writeFile(`${folderPath}/${pageName}.ts` , ts(pageName), { flag: 'wx' }, (err) => {
      if (err) console.log(err)
      console.log(colors.green(`Created Typescript file: ${pageName}/${pageName}.ts`))
    })

    fs.writeFile(`${folderPath}/${pageName}.vue` , html(pageName), { flag: 'wx' }, (err) => {
      if (err) console.log(err)
      console.log(colors.green(`Created Vue file: ${folderPath}/${pageName}.vue`))
    })

    fs.writeFile(`${folderPath}/${pageName}.scss` , scss(pageName), { flag: 'wx' }, (err) => {
      if (err) console.log(err)
      console.log(colors.green(`Created Scss file: ${pageName}/${pageName}.scss`))
    })

    return
  }

  console.log(colors.yellow(`A folder by the name "${pageName}" already exists in that location.`))
}


module.exports = {
  validateArgs,
  createNewPage
}