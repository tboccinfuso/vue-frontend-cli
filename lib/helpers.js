module.exports = {
  currentProcess: {},
  init: (process) => {
    this.currentProcess = process
  },
  processArgs: () => {
    const Args = this.currentProcess.argv.slice(2)
    return {
      args: Args,
      numberOfArgs: Args.length
    }
  },
  camelCaseWord: (word) => {
    return word.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
  },
  pascalCaseWord: (word) => {
    return word.replace(/([A-Z]+)/g, "$1").replace(/^./, (pageName) => { return pageName.toUpperCase() })
  },
  toId: (word) => {
    const words = word.toString().split(/(?=[A-Z])/)
    let snake_cased = ''

    for (let i = 0; i < words.length; i++) {
      if (i != 0) {
        words[i] = '_' + words[i]
      }

      snake_cased += words[i].toLowerCase()
    }

    return snake_cased
  },
  ucf: (word) => {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }
}