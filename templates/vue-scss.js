const helpers = require('../lib/helpers')

module.exports = (pageName) => {

  const template =
  `#${helpers.toId(pageName)} {

}`

  return template
}
