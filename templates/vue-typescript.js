const helpers = require('../lib/helpers')

module.exports = (pageName) => {

  const template =
  `import { Component, Vue } from 'vue-property-decorator'

@Component({

})
export default class ${helpers.ucf(pageName)} extends Vue {

}`

  return template
}
