const helpers = require('../lib/helpers')

module.exports = (pageName) => {

  const template =
  `<template>
  <div id="${helpers.toId(pageName)}">

  </div>
</template>

<script lang="ts" src="./${pageName}.ts"></script>

<style lang="scss">
  @import '${pageName}.scss';
</style>`

  return template
}
