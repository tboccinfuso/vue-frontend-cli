#!/usr/bin/env node
const program = require('commander')
const helpers = require('../lib/helpers')

// Commands
const newComponent = require('../commands/new-component')

program.version('0.1.0')

// Options
program
  .option('-d, --debug', 'output debug info')

// Commands
program
  .command('nc <PageName> <path>')
  .description('Create a new Vue page / component folder at the given path. Includes Vue, Typescript & Scss files.')
  .action((pageName, path) => {
    pageName = helpers.pascalCaseWord(pageName)

    helpers.init(process)

    const validArgs = newComponent.validateArgs(helpers.processArgs(), pageName)

    if (!validArgs) return

    newComponent.createNewPage(pageName, path)
  })

program.parse(process.argv)

if (program.debug) console.log(program.opts())

if (process.argv.length <= 2) console.log( program.helpInformation() )
