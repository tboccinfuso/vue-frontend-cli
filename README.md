# Simple cli

A simple cli to speed up Vue / TS development.

## commands

### nc

This command is used to create a new Vue page / component folder at the given path. Includes Vue, Typescript & Scss files.

`node ./bin/frontend.js nc TestPage /home`

There will now be a new folder located at /home/TestPage. Inside this folder it will also include the matching files with templated code.
